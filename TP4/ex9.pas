program Ejercicio9;
{Enunciado: Confeccionar dos funciones MesAnterior y MesSiguiente, ;que dado el nombre del mes
devuelvan el nombre del mes anterior y del siguiente respectivamente. Escribir una
función Ayer y otra Mañana, que dado el nombre de un día devuelva el nombre del día
de ayer y de mañana respectivamente. Ej.: Ayer(martes) da lunes. }
var
	day, month: String;

function MesAnterior(m:String): String;
	var mes :String;
	begin
		case m of
			'Enero', 'enero': 			mes:= 'Diciembre';
			'Febrero', 'febrero':	 	mes:= 'Enero';
			'Marzo', 'marzo': 			mes:= 'Febrero';
			'Abril', 'abril': 			mes:= 'Marzo';
			'Mayo', 'mayo': 			mes:= 'Abril';
			'Junio', 'junio':	 		mes:= 'Mayo';
			'Julio', 'julio': 			mes:= 'Junio';
			'Agosto', 'agosto':			mes:= 'Julio';
			'Septiembre', 'septiembre': mes:= 'Agosto';
			'Octubre', 'octubre': 		mes:= 'Septiembre';
			'Noviembre', 'noviembre': 	mes:= 'Octubre';
			'Diciembre', 'diciembre': 	mes:= 'Noviembre';
			else 
				mes:='none';
		end;
		MesAnterior := mes;
	end;

function MesSiguiente(m:String): String;
	var mes :String;
	begin
		case m of
			'Enero', 'enero': 			mes:= 'Febrero';
			'Febrero', 'febrero':	 	mes:= 'Marzo';
			'Marzo', 'marzo': 			mes:= 'Abril';
			'Abril', 'abril': 			mes:= 'Mayo';
			'Mayo', 'mayo': 			mes:= 'Junio';
			'Junio', 'junio':	 		mes:= 'Julio';
			'Julio', 'julio': 			mes:= 'Agosto';
			'Agosto', 'agosto':			mes:= 'Septiembre';
			'Septiembre', 'septiembre': mes:= 'Octubre';
			'Octubre', 'octubre': 		mes:= 'Noviembre';
			'Noviembre', 'noviembre': 	mes:= 'Diciembre';
			'Diciembre', 'diciembre': 	mes:= 'Enero';
			else 
				mes:='none';
		end;
		MesSiguiente := mes;
	end;

function Yesterday(d:String): String;
	var dia:String;
	begin
		case d of
			'Lunes', 'lunes': 			dia:='Domingo';
			'Martes', 'martes': 		dia:='Lunes';
			'Miercoles', 'miercoles': 	dia:='Martes';
			'Jueves', 'jueves': 		dia:='Miercoles';
			'Viernes', 'viernes':		dia:='Jueves';
			'Sabado', 'sabado': 		dia:='Viernes';
			'Domingo', 'domingo': 		dia:='Sabado';
			else 
				dia:='none';
		end;
		Yesterday := dia;
	end;

function Morning(d:String): String;
	var dia:String;
	begin
		case d of
			'Lunes', 'lunes': 			dia:='Martes';
			'Martes', 'martes': 		dia:='Miercoles';
			'Miercoles', 'miercoles': 	dia:='Jueves';
			'Jueves', 'jueves': 		dia:='Viernes';
			'Viernes', 'viernes':		dia:='Sabado';
			'Sabado', 'sabado': 		dia:='Domingo';
			'Domingo', 'domingo': 		dia:='Lunes';
			else 
				dia:='none';
		end;
		Morning := dia;
	end;

//Main	
begin
	write('Ingrese un mes(Ej: Enero): ');
	readln(month);
	write('Ingrese un dia(Ej: Lunes): ');
	readln(day);

	writeln('El mes anterior de ', month,' es: ', MesAnterior(month));
	writeln('El mes siguiente de ', month,' es: ', MesSiguiente(month));

	writeln('El dia anterior de ', day,' es: ', Yesterday(day));
	writeln('El dia siguiente de ', day,' es: ', Morning(day));

	readln;
end.
