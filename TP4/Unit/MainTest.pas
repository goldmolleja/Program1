program MainTest;
{Prgrama principal para un test de la Libreria.pas}
uses Libreria;

var
	numero, digito :Integer;

begin
	//HolaMundo;
	write('Ingrese un numero: ');
	readln(numero);
	write('Ingrese un digito: ');
	readln(digito);

	if BuscarDigito(numero, digito) then 
		writeln(':) El digito [',digito,'] fue encontrado dentro del numero [',numero,']')
	else
		writeln(':( El digito [',digito,'] no fue encontrado dentro del numero [',numero,']');

	//readln; //Opcional ya que mi consola lo genera
end.
