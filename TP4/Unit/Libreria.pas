unit Libreria;

interface

	procedure HolaMundo();
	function BuscarDigito( num, dig:integer ) :Boolean;
	function Factorial( numero:Integer ) :Integer;
	function ContieneDigito(n:Integer; d:Integer): Boolean;
	function MesAnterior(m:String): String;
	function MesSiguiente(m:String): String;
	function Yesterday(d:String): String;
	function Morning(d:String): String;
	function NumeroMes(month:String) :Byte;
	
implementation

	procedure HolaMundo();
		begin
			writeln('Hello Word !!!');
		end;

	{función del ejercicio 5 del trabajo práctico 4}
	function BuscarDigito( num, dig:integer ) :Boolean;
		var wanted :Boolean;
		begin
			wanted := false;
			repeat
				if (num mod 10) = dig then wanted := true;
				num := num div 10;
			until (num = 0) or wanted;
			BuscarDigito:= wanted; //Retornamos el resultado de la buscada del digito dentro del numero dado
		end;

	{función del ejercicio 7 del trabajo práctico 4}
	function Factorial( numero:Integer ) :Integer;
		var elnumero, i: Integer;
		begin
			elnumero := 1;
			if numero=0 then Factorial:=1;

			for i := 1 to numero do
				begin
					elnumero := elnumero * i;
				end;
			Factorial:=elnumero;
		end;

	{función del ejercicio 6 del trabajo práctico 4}
	function ContieneDigito(n:Integer; d:Integer): Boolean;
		var
			resto :Integer;
			contine :Boolean;
		begin
			contine:= false;
			repeat
				resto:= n mod 10;
				n := n div 10;
				if (resto = d) then contine:= true;
			until (n=0) or (contine=true);

			ContieneDigito:=contine;
		end;

	{función del ejercicio 9 del trabajo práctico 4}
	function MesAnterior(m:String): String;
		var mes :String;
		begin
			case m of
				'Enero', 'enero': 			mes:= 'Diciembre';
				'Febrero', 'febrero':	 	mes:= 'Enero';
				'Marzo', 'marzo': 			mes:= 'Febrero';
				'Abril', 'abril': 			mes:= 'Marzo';
				'Mayo', 'mayo': 			mes:= 'Abril';
				'Junio', 'junio':	 		mes:= 'Mayo';
				'Julio', 'julio': 			mes:= 'Junio';
				'Agosto', 'agosto':			mes:= 'Julio';
				'Septiembre', 'septiembre': mes:= 'Agosto';
				'Octubre', 'octubre': 		mes:= 'Septiembre';
				'Noviembre', 'noviembre': 	mes:= 'Octubre';
				'Diciembre', 'diciembre': 	mes:= 'Noviembre';
				else 
					mes:='none';
			end;
			MesAnterior := mes;
		end;

	{función del ejercicio 9 del trabajo práctico 4}
	function MesSiguiente(m:String): String;
		var mes :String;
		begin
			case m of
				'Enero', 'enero': 			mes:= 'Febrero';
				'Febrero', 'febrero':	 	mes:= 'Marzo';
				'Marzo', 'marzo': 			mes:= 'Abril';
				'Abril', 'abril': 			mes:= 'Mayo';
				'Mayo', 'mayo': 			mes:= 'Junio';
				'Junio', 'junio':	 		mes:= 'Julio';
				'Julio', 'julio': 			mes:= 'Agosto';
				'Agosto', 'agosto':			mes:= 'Septiembre';
				'Septiembre', 'septiembre': mes:= 'Octubre';
				'Octubre', 'octubre': 		mes:= 'Noviembre';
				'Noviembre', 'noviembre': 	mes:= 'Diciembre';
				'Diciembre', 'diciembre': 	mes:= 'Enero';
				else 
					mes:='none';
			end;
			MesSiguiente := mes;
		end;

	{función del ejercicio 9 del trabajo práctico 4}
	function Yesterday(d:String): String;
		var dia:String;
		begin
			case d of
				'Lunes', 'lunes': 			dia:='Domingo';
				'Martes', 'martes': 		dia:='Lunes';
				'Miercoles', 'miercoles': 	dia:='Martes';
				'Jueves', 'jueves': 		dia:='Miercoles';
				'Viernes', 'viernes':		dia:='Jueves';
				'Sabado', 'sabado': 		dia:='Viernes';
				'Domingo', 'domingo': 		dia:='Sabado';
				else 
					dia:='none';
			end;
			Yesterday := dia;
		end;

	{función del ejercicio 9 del trabajo práctico 4}
	function Morning(d:String): String;
		var dia:String;
		begin
			case d of
				'Lunes', 'lunes': 			dia:='Martes';
				'Martes', 'martes': 		dia:='Miercoles';
				'Miercoles', 'miercoles': 	dia:='Jueves';
				'Jueves', 'jueves': 		dia:='Viernes';
				'Viernes', 'viernes':		dia:='Sabado';
				'Sabado', 'sabado': 		dia:='Domingo';
				'Domingo', 'domingo': 		dia:='Lunes';
				else 
					dia:='none';
			end;
			Morning := dia;
		end;

	{función del ejercicio 10 del trabajo práctico 4}
	function NumeroMes(month:String) :Byte;
		var numberOfMonth :Byte;
		begin
			case month of
				'Enero', 'enero': 			numberOfMonth := 01;
				'Febrero', 'febrero':	 	numberOfMonth := 02;
				'Marzo', 'marzo': 			numberOfMonth := 03;
				'Abril', 'abril': 			numberOfMonth := 04;
				'Mayo', 'mayo': 			numberOfMonth := 05;
				'Junio', 'junio':	 		numberOfMonth := 06;
				'Julio', 'julio': 			numberOfMonth := 07;
				'Agosto', 'agosto':			numberOfMonth := 08;
				'Septiembre', 'septiembre': numberOfMonth := 09;
				'Octubre', 'octubre': 		numberOfMonth := 10;
				'Noviembre', 'noviembre': 	numberOfMonth := 11;
				'Diciembre', 'diciembre': 	numberOfMonth := 12;
				else 
					numberOfMonth := 69;
			end;
			NumeroMes := numberOfMonth;
		end;
end.
