# Trabajo Práctico N°4 
## Funciones y procedimientos
---
##### 1. Responder
 - a) ¿Qué es la programación modular? 
 - b) ¿Cuáles son las ventajas de la programación modular? 
 - c) ¿Qué es el diseño top-down?
 - d) ¿Qué es un procedimiento?
 - e) ¿Qué es una función y en qué se diferencia de un procedimiento?
 - f) ¿Qué entiende por argumentos o parámetros? ¿Cuál es su propósito?
 - g) ¿Qué se entiende por una llamada a función? ¿Desde qué partes de un programa sepuede llamar a un procedimiento o función?
 - h) ¿Qué son los parámetro formales? ¿Qué son los parámetros actuales? ¿Cuál es la relación entre ellos?
 - i) ¿Pueden coincidir los nombres de los argumentos formales dentro de una función o procedimiento con los nombres de otras variables definidas fuera de la función? Explicarlo.
 - j) ¿Pueden coincidir los nombres de los argumentos formales dentro de una función o procedimiento con los nombres de otras variables definidas dentro de la función? Explicarlo.
 - k) ¿Qué es el ámbito de una variable?
 - l) ¿Para qué se utilizan las variables globales? ¿Es conveniente utilizarlas? ¿Por qué?

##### 2. Escribir una función que permita elevar un número real a una potencia entera. 
En otras palabras, deseamos evaluar la fórmula: $$y = x^n$$ , donde (y) y (x) son variables reales y (n) una variable entera.

##### 3. Escribir un programa completo en Delphi que lea los valores de (x) y (n), evaluar la fórmula indicada en el ejercicio anterior y escribir a continuación el resultado. 
Comprobar el programa con los siguientes datos:

| x | n |
| - | - |
| 2 | 3 |
| 2 | -5 |
| -3 | 3 |
| -3 | -5 |
| 1.5 | 3 |
| 0.2 | -5 |

##### 4. Escribir un programa que tenga una subprograma por cada uno de los siguientes problemas:
- a) Hallar las raíces de una ecuación cuadrática mediante la fórmula de Baskara.
- b) Calcular la edad de una persona dada la fecha de nacimiento y la fecha actual.
- c) Calcular el area de un rectángulo.

##### 5. Escribir una subprograma cuyos parámetros sean un entero positivo y un dígito y determine si el entero contiene al dígito.
#
##### 6. Escriba un programa que lea un dígito D y tabule todos los números enteros de 1 al 100 tales que la representación decimal del número, y cuadrado y su cubo contengan todos el dígito.
Por ejemplo, si D=1, entonces 13 es el número que buscamos ya que: 13,169 y 2197 contienen el 1.

##### 7. Escribir una subprograma que calcule el factorial de un número entero. El programa principal es el que lee el número, hacer otra función que controle que el número ingresado sea positivo y luego de calcular el factorial imprime: 
#
```sh
Número ! = valor factorial
```
##### 8. Escribir un procedimiento en Delphi que transforme una fecha dada en días, meses y años en un longint. Ej.: si la fecha es 25 mayo 1998 entonces devolver 25051998, si la fecha es 5 junio 1990 entonces devolver 5061990. 
#
##### 9. Confeccionar dos funciones MesAnterior y MesSiguiente, ;que dado el nombre del mes devuelvan el nombre del mes anterior y del siguiente respectivamente. Escribir una función Ayer y otra Mañana, que dado el nombre de un día devuelva el nombre del día de ayer y de mañana respectivamente. Ej.: Ayer(martes) da lunes.
#
##### 10. Desarrollar una función NúmeroMes, que dado un nombre de un mes devuelva el número asociado a él. Ej. NúmeroMes(agosto) devuelve 8. 
#
##### 11. Escribir la función NombreMes, que sea la inversa de la función anterior.
#
##### 12. Considere el siguiente programa en Lenguaje Delphi:
#
```sh
PROGRAM Mayus;
CONST offset = ord(‘0’);
VAR car: char;
FUNCTION EsMayuscula(c:char): boolean;
Begin
    EsMayuscula:= (c >= ‘A’) and ( c <= ‘Z’)
End;
FUNCTION EsMinuscula (c: char): char;
Begin
    EsMinuscula:= ( car >= ‘a’) and ( car <= ‘z’)
End;
FUNCTION Amayúscula ( c:char): char;
Const
    Offset = ord(‘A’) – ord(‘a’);
Var car:char;
Begin
    If EsMayuscula( c ) then
        Car:= c
    Else
        Car:=chr( ord( c ) + offset);
    Amayuscula:= car;
End;
FUNCTION EsVocal ( c: char): boolean;
Var minus:char;
    FUNCTION AMinúscula ( c:char): char;
    Const
        Offset = ord(‘A’) – ord(‘a’);
    Begin
        If EsMayuscula( c ) then
            Minus:= chr( ord (c ) + offset)
        Else
            Minus:= c;
        Aminuscula:=minus;

    End;
Begin
    Minus:= Aminuscula(C);
    EsVocal:= ( minus = ‘a’ ) or (minus = ‘e’) or (minus = ‘i’) or
                (minus = ‘o’) or (minus = ‘u’)
End;
Begin {Ppal}
    Writeln(‘Ingrese una frase terminada en punto “.”`);
    Repeat
        Read( car);
    If EsMayúscula(car) or EsMinuscula(car) then
        If EsVocal(car) then write(Amayuscual(car))
        Else write(car);
    Until ( car = ‘.’);
End.
```
> Complete la siguiente tabla indicando para cada bloque el entorno de referencia de cada identificador.

|  | Mayus | EsMayuscula | EsMinuscula | Amayuscula | Evsocal | Aminuscula |
| - | - | - | - | - | - | - |
| Entorno Global | - | - | - | - | - | - |
| Entorno Local | - | - | - | - | - | - |