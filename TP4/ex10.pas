program Ejercicio10;
{Enunciado:  Desarrollar una función NúmeroMes, que dado un nombre de un mes devuelva el
número asociado a él. Ej. NúmeroMes(agosto) devuelve 8.}
var
	mes :String;

function NumeroMes(month:String) :Byte;
	var numberOfMonth :Byte;
	begin
		case month of
			'Enero', 'enero': 			numberOfMonth := 01;
			'Febrero', 'febrero':	 	numberOfMonth := 02;
			'Marzo', 'marzo': 			numberOfMonth := 03;
			'Abril', 'abril': 			numberOfMonth := 04;
			'Mayo', 'mayo': 			numberOfMonth := 05;
			'Junio', 'junio':	 		numberOfMonth := 06;
			'Julio', 'julio': 			numberOfMonth := 07;
			'Agosto', 'agosto':			numberOfMonth := 08;
			'Septiembre', 'septiembre': numberOfMonth := 09;
			'Octubre', 'octubre': 		numberOfMonth := 10;
			'Noviembre', 'noviembre': 	numberOfMonth := 11;
			'Diciembre', 'diciembre': 	numberOfMonth := 12;
			else 
				numberOfMonth := 69;
		end;
		NumeroMes := numberOfMonth;
	end;

begin
	write('Ingrese un mes (ej: Enero): ');
	readln(mes);

	writeln('El numero de ', mes,' es: ', NumeroMes(mes));
end.
