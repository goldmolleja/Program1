program Ejercicio5;
{Enunciado:Escribir una subprograma cuyos parámetros sean un entero positivo y un dígito y determine si el entero contiene al dígito. }

var
	numero, digito :Integer;

function ContieneDigito(num,dig:Integer): Boolean;
	var encontre :Boolean;
	begin
		encontre := false;
		repeat
			if (num mod 10) = dig then encontre := true;
			num := num div 10;
		until (num = 0) or encontre;
		ContieneDigito := encontre; //Return
	end;


begin
	write('Ingrese un numero: ');	readln(numero);
	write('Ingrese un digito: ');	readln(digito);

	if ContieneDigito(numero, digito) then 
		write(':) El digito [',digito,'] esta contenido en el numero [',numero,']')
	else
		write(':( El digito [',digito,'] no esta contenido en el numero [',numero,']');
	readln;
end.
