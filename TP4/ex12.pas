{Considere el siguiente programa en Lenguaje Delphi:}
PROGRAM Mayus;

CONST offset = ord(‘0’);

VAR car: char;

FUNCTION EsMayuscula(c:char): boolean;
	Begin
		EsMayuscula:= (c >= ‘A’) and ( c <= ‘Z’)
	End;

FUNCTION EsMinuscula (c: char): char;
	Begin
		EsMinuscula:= ( car >= ‘a’) and ( car <= ‘z’)
	End;

// Instituto Superior Juan XXIII
// Analista en Sistemas 3

FUNCTION Amayúscula ( c:char): char;
	Const
		Offset = ord(‘A’) – ord(‘a’);
	Var car:char;
	Begin
		If EsMayuscula( c ) then
			Car:= c
		Else
			Car:=chr( ord( c ) + offset);
		Amayuscula:= car;
	End;

FUNCTION EsVocal ( c: char): boolean;
	Var minus:char;
	FUNCTION AMinúscula ( c:char): char;
		Const
			Offset = ord(‘A’) – ord(‘a’);
		Begin
			If EsMayuscula( c ) then
				Minus:= chr( ord (c ) + offset)
			Else
				Minus:= c;
			Aminuscula:=minus;
		End;
	Begin
		Minus:= Aminuscula(C);
		EsVocal:= ( minus = ‘a’ ) or (minus = ‘e’) or (minus = ‘i’) or (minus = ‘o’) or (minus = ‘u’)
	End;

Begin {Ppal}
	Writeln(‘Ingrese una frase terminada en punto “.”`);
	Repeat
		Read( car);
		If EsMayúscula(car) or EsMinuscula(car) then
			If EsVocal(car) then 
				write(Amayuscual(car))
		Else write(car);
	Until ( car = ‘.’);
End. 

{
	Complete la siguiente tabla indicando para cada bloque el entorno de referencia de cada identificador
				Mayus 	EsMayuscula		EsMinuscula		Amayuscula 		Es vocal	Aminuscula
	Entorno
	Global		[ok]		[ok]			[ok]						  [ok]

	Entorno
	Local													[ok]						[ok]
}