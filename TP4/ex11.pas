program Ejercicio11;
{Enunciado:  Desarrollar una función NombreMes, que dado un número de un mes devuelva el
nombre asociado a él. Ej. NombreMes(8) devuelve Agosto.}
var
	mes :Byte;

function NombreMes (num:Byte) :String;
	var nickMonth :String;
	begin
		case num of
			01: nickMonth := 'Enero';
			02: nickMonth := 'Febrero';
			03: nickMonth := 'Marzo';
			04: nickMonth := 'Abril';
			05: nickMonth := 'Mayo';
			06: nickMonth := 'Junio';
			07: nickMonth := 'Julio';
			08: nickMonth := 'Agosto';
			09: nickMonth := 'Septiembre';
			10: nickMonth := 'Octubre';
			11: nickMonth := 'Noviembre';
			12: nickMonth := 'Diciembre';
			else 
				nickMonth := 'Sin nombre';
		end;
		NombreMes := nickMonth;
	end;

begin
	write('Ingrese un numero [1..12] (ej: 4): ');
	readln(mes);

	writeln('El numero de ', mes,' es: ', NombreMes(mes));
end.
