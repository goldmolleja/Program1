program Ejercicio6;
{Enunciado: Escriba un programa que lea un dígito D y tabule todos los números enteros de 1 al 100
tales que la representación decimal del número, y cuadrado y su cubo contengan todos
el dígito.}

var
	digito, n, cuadrado, cubo :Integer;

function ContieneDigito(n:Integer; d:Integer): Boolean;
	var
		resto :Integer;
		contine :Boolean;
	begin
		contine:= false;
		repeat
			resto:= n mod 10;
			n := n div 10;
			if (resto = d) then contine:= true;
		until (n=0) or (contine=true);

		ContieneDigito:=contine;
	end;

begin
	repeat
		write('Ingrese el digito (0..9)');
		readln(digito);	
	until (digito>=0) and (digito<=9);
	
	for n := 1 to 100 do
		begin
			cuadrado:= sqr(n);
			cubo:= cuadrado * n;
			if ContieneDigito(n,digito) and ContieneDigito(cuadrado,digito) and ContieneDigito(cubo,digito) then 
				writeln('n: ', n,' - cuadrado: ', cuadrado,' - cubo: ', cubo);
		end;
end.

