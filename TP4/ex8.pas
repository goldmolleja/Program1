program Ejercicio8;
{Enunciado: Escribir un procedimiento en Delphi que transforme una fecha dada en días, meses y
años en un longint. Ej.: si la fecha es 25 mayo 1998 entonces devolver 25051998, si la
fecha es 5 junio 1990 entonces devolver 5061990. }

var
	day, month, year: String;

procedure ConvertirFecha(day, month, year:String);
	var
		mes: String;
	begin
		case month of
			'Enero', 'enero': 			mes:= '01';
			'Febrero', 'febrero':	 	mes:= '02';
			'Marzo', 'marzo': 			mes:= '03';
			'Abril', 'abril': 			mes:= '04';
			'Mayo', 'mayo': 			mes:= '05';
			'Junio', 'junio':	 		mes:= '06';
			'Julio', 'julio': 			mes:= '07';
			'Agosto', 'agosto':			mes:= '08';
			'Septiembre', 'septiembre': mes:= '09';
			'Octubre', 'octubre': 		mes:= '10';
			'Noviembre', 'noviembre': 	mes:= '11';
			'Diciembre', 'diciembre': 	mes:= '12';
			else 
				mes:='none';
		end;

		writeln('La fecha convertida es: ', day+mes+year);
	end;

begin
	write('Ingrese el dia: ');
	readln(day);
	write('Ingrese el mes(ej: Enero): ');
	readln(month);
	write('Ingrese el anio: ');
	readln(year);

	ConvertirFecha(day,month,year);

	readln;
end.
