program Ejercicio7;
{Enunciado: Escribir una subprograma que calcule el factorial de un número entero. 
El programa principal es el que lee el número, hacer otra función que controle que el número
ingresado sea positivo y luego de calcular el factorial imprime: }

var
		numero :Integer;

function Factorial(numero:Integer): Integer;
	var elnumero, i: Integer;
	begin
		elnumero := 1;
		if numero=0 then Factorial:=1;

		for i := 1 to numero do
			begin
				elnumero := elnumero * i;
			end;
		Factorial:=elnumero;
	end;

begin
	repeat
		write('Ingrese un numero(mayor o igual a cero): ');
		readln(numero);		
	until (numero >= 0);


	writeln('El factorial de [', numero,'!] es : ', Factorial(numero));
	readln;
end.
